<?php
session_start();

// Vérifiez si l'utilisateur a soumis le formulaire de connexion
if(isset($_POST['submit'])){
    // Récupérez les données du formulaire
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Vérifiez les informations d'identification de l'utilisateur
    // Remplacez cette partie par votre propre méthode de vérification, comme une requête SQL ou une vérification de hachage
    if($username == "admin" && $password == "password"){
        // Enregistrez les informations d'identification de l'utilisateur dans la session
        $_SESSION['loggedin'] = true;
        $_SESSION['username'] = $username;
        $_SESSION['role'] = "admin";

        // Redirigez l'utilisateur vers la page protégée
        header("Location: panel.php");
    } else {
        echo "Nom d'utilisateur ou mot de passe incorrect.";
    }
}
?>
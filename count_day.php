<?php
    // établir une connexion à la base de données
    $host = "hostname";
    $username = "username";
    $password = "password";
    $dbname = "dbname";
    $conn = mysqli_connect($host, $username, $password, $dbname);

    // vérifier la connexion
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    // vérifier si la table de compteur existe
    $table_name = "counter_day";
    $check_table = mysqli_query($conn, "SHOW TABLES LIKE '$table_name'");
    if(mysqli_num_rows($check_table) == 0) {
        // si elle n'existe pas, la créer
        $create_table = "CREATE TABLE $table_name (date DATE PRIMARY KEY, count INT)";
        if(mysqli_query($conn, $create_table)) {
            echo "Table $table_name created successfully.";
        } else {
            echo "Error creating table $table_name: " . mysqli_error($conn);
        }
    }

    // récupérer la date actuelle
    $current_date = date("Y-m-d");

    // vérifier si un compteur existe pour cette date
    $check_counter = mysqli_query($conn, "SELECT count FROM $table_name WHERE date = '$current_date'");
    if(mysqli_num_rows($check_counter) == 0) {
        // si non, ajouter un compteur avec une valeur initiale de 1
        $add_counter = "INSERT INTO $table_name (date, count) VALUES ('$current_date', 1)";
        if(mysqli_query($conn, $add_counter)) {
            echo "Counter added successfully.";
        } else {
            echo "Error adding counter: " . mysqli_error($conn);
        }
    } else {
        // sinon incrémenter le compteur existant
        $update_counter = "UPDATE $table_name SET count = count + 1 WHERE date = '$current_date'";
        if(mysqli_query($conn, $update_counter)) {
            echo "Counter updated successfully.";
        } else {
            echo "Error updating counter: " . mysqli_error($conn);
        }
    }

    // récupérer la valeur actuelle du compteur
    $get_counter = mysqli_query($conn, "SELECT count FROM $table_name WHERE date = '$current_date'");
    $counter_day = mysqli_fetch_assoc($get_counter);

    // fermer la connexion à la base de données
    mysqli_close($conn);

    // afficher le compteur
    echo "Nombre de visites aujourd'hui : " . $counter_day['count'];
?>
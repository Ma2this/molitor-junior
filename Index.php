<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Molitor Junior</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="style.css" rel="stylesheet">
       
    </head>
    <body>
<!-- Page d'acceuil - écran-->
    <div id="container-1">
        <!-- Nav barre-->
        <div id="nav">
            <h1 id="molitor-junior">Molitor Junior</h1>
            <button class="user"type="button" ><a href="../login/login.php"><img  class="user" src="../image/user.png"/></a></button>
        </div>
        <div id="annonce">
            <h3 id="title-annonce"> C'est les soldes profitez de -70%</h3>
        </div>
        <img id="enfant-background" src="../image/Enfant.jpg">
    </div>

    <div id="container-reseaux">

        
          
        
        <div id="TextAndDeco">
            <h1 class="title-bloc">Réseaux Sociaux</h1>
            <div class="deco"></div>
        </div>
        
        <div id="container-cards">
            <div class="card"></div>
            <div class="card"></div>
            <div class="card"></div>
            <div class="card"></div>
            <div class="card"></div>
            <div class="card"></div>
            <div class="card"></div>
            <div class="card"></div>
            <div class="card"></div>
        </div>
        
    </div>

    <div id="container-article">
        <div id="TextAndDeco">
            <h1 class="title-bloc">Nos Rubriques</h1>
            <div class="deco"></div>
        </div>

        <div id="all-rubrique">
            <div class="rubrique">
                <img class="img-rubrique" src="../image/fille.jpg">
                <div class="box-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
    
            <div class="rubrique">
                <img class="img-rubrique" src="../image/garçon.jpg">
                <div class="box-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
    
            <div class="rubrique">
                <img class="img-rubrique" src="../image/access.jpg">
                <div class="box-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
        </div>
        
    </div>

    <div id="container-info">
        <div id="TextAndDeco">
            <h1 class="title-bloc">Informations</h1>
            <div class="deco"></div>
        </div>

        <div id="information-flex">

            <div id="horaire">
                <head>
                    <title>Titre du document</title>
                  </head>
                  <body>
                    <table>
                      <tr>
                        <th>Horaires</th>
                      </tr>
                      <tr>
                        <td>Lundi</td>
                        <td>Fermé</td>
                      </tr>
                      <tr>
                        <td>Mardi</td>
                        <td>10h-12h, 14h-19h</td>
                      </tr>
                      <tr>
                        <td>Mercredi</td>
                        <td>10h-12h, 14h-19h</td>
                      </tr>
                      <tr>
                        <td>Jeudi</td>
                        <td>10h-12h, 14h-19h</td>
                      </tr>
                      <tr>
                        <td>Vendredi</td>
                        <td>10h-12h, 14h-19h</td>
                      </tr>
                      <tr>
                        <td>Samedi</td>
                        <td>10h-12h, 14h-19h</td>
                      </tr>
                    </table>
            </div>

            <div id="map">
                <div id="btn-flex">
                    <button class="btn" id="btn-08">Charleville</button>
                    <button class="btn" id="btn-51">Epernay</button>
                </div>
                
                
                <iframe class="gmap" id="n08" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2576.7398510184785!2d4.71671481592343!3d49.77215034388604!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47ea0f5ed22b753d%3A0x7e705b5ca29d9419!2sMOLITOR%20JUNIOR!5e0!3m2!1sfr!2sfr!4v1673515275954!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
           <!--    <iframe class="gmap" id="n51" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d449.13916834279024!2d3.953110061421786!3d49.04551623967941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e96b51c30e374b%3A0xd1ff4a8b1c931fef!2sMolitor%20Junior!5e0!3m2!1sfr!2sfr!4v1673960432922!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe> -->
            </div>

        </div>

    </div>

    <footer>
        <div class="col">
            <h2 class="title-f">Informations</h2>
            <div class="deco-f"></div>
            <a class="lien">Contactez nous</a>
            <a class="lien">Mentions légals</a>
            <a class="lien">Site map</a>
        </div>

        <div class="col">
            <h2 class="title-f">Contacts</h2>
            <div class="deco-f"></div>
            <a class="lien">Instagram</a>
            <a class="lien">Facebook</a>
        </div>

        <div class="col">
            <h2 class="title-f">A propos</h2>
            <div class="deco-f"></div>
            <a class="text-f">molitor-junior chassure enfant, fill et garçcon <br>à la pointe de la mode, Profitez de -70% de réduction !</a>
            <a class="text-f">10 rue Dr Verron, 51200 Epernay</a>
            <a class="text-f">03 56 55 50 56</a>
        </div>
       
    </footer>

      
	<script src="script.js"></script>


    </body>
</html>
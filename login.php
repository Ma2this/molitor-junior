<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style_login.css" rel="stylesheet">
    <title>Molitor Junior login</title>
</head>
<body>

    <div id="container-1">
        <!-- Nav barre-->
        <div id="nav">
            <h1 id="molitor-junior">Molitor Junior</h1>
            <button class="user"  type="button" ><img  class="user" src="../image/user.png"/></button>
        </div>

        <div class="container">
            <form>
                <img src="../image/user-login.png" alt="Profile-pic">
                <div class="input_box">
                    <input type="text" name="username" placeholder="User Name">
                </div>
                <div class="input_box">
                    <input type="password" name="password" placeholder="Password">
                </div>
                <input type="submit" name="submit" value="Login" class="submit_btn">
                <br><a href="#" class="for_pass">Forgot Password</a>
            </form>
        </div>
    
</body>
</html>